<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ include file="commons/Taglibs.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'showList.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript">
	function updateById(userId){
	document.getElementById("userId").value=userId;
	alert(document.getElementById("userId").value);
	//location.href='UserManageAction!selectUserById?userId='+userId;
	document.getElementById("form1").submit();
	}
	
	function deleteUserById(userId){
		location.href='UserManageAction!deleteUserById?userId='+userId;
	}
	</script>

  </head>

  <body>
  <form action="UserManageAction!selectUserById" method="post" name="form1" id="form1">
  <input type="hidden" value="" name="userId" id="userId"/>
  </form>
   <table border="1" cellpadding="10" cellspacing="0">
   <tr><td>ID</td><td>姓名</td><td>性别</td><td>年龄</td><td>时间</td><td>操作</td></tr>
   <c:forEach items="${list}" var="list">
   <tr><td>${list.userId}</td><td>${list.userName}</td><td>${list.userSex}</td><td>${list.userAge}</td><td><fmt:formatDate value="${list.userTime}" pattern="yyyy-MM-dd"/></td><td><a href="javascript:updateById(${list.userId});">修改</a><a href="javascript:deleteUserById(${list.userId});">删除</a></td></tr>
   </c:forEach>
   </table>
  </body>
</html>
