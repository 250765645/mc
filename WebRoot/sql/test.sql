/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50537
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50537
File Encoding         : 65001

Date: 2015-01-12 17:21:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `carinfo`
-- ----------------------------
DROP TABLE IF EXISTS `carinfo`;
CREATE TABLE `carinfo` (
  `carId` int(11) NOT NULL AUTO_INCREMENT,
  `carName` varchar(20) DEFAULT NULL,
  `carColor` varchar(2) DEFAULT NULL,
  `carPrice` double DEFAULT NULL,
  `carTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`carId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of carinfo
-- ----------------------------
INSERT INTO `carinfo` VALUES ('1', '保时捷', '红色', '102', '2014-12-24 09:50:10');

-- ----------------------------
-- Table structure for `dept`
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept` (
  `deptId` int(11) NOT NULL AUTO_INCREMENT,
  `deptName` varchar(20) NOT NULL,
  `deptLoc` varchar(20) NOT NULL,
  PRIMARY KEY (`deptId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES ('1', '研发部', '武汉');
INSERT INTO `dept` VALUES ('3', '行政部', '天津');
INSERT INTO `dept` VALUES ('4', '财务部', '新疆');
INSERT INTO `dept` VALUES ('5', '销售部', '上海');

-- ----------------------------
-- Table structure for `emp`
-- ----------------------------
DROP TABLE IF EXISTS `emp`;
CREATE TABLE `emp` (
  `empId` int(11) NOT NULL AUTO_INCREMENT COMMENT '员工的ID主键自动增长',
  `empName` varchar(10) DEFAULT NULL COMMENT '员工名字长度为10',
  `empPass` varchar(10) DEFAULT NULL COMMENT '员工密码长度最大为10',
  `deptId` int(11) DEFAULT NULL COMMENT '部门ID为外键为部门表的',
  `empTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '录入系统时间',
  PRIMARY KEY (`empId`),
  KEY `FK_deptId` (`deptId`),
  CONSTRAINT `FK_deptId` FOREIGN KEY (`deptId`) REFERENCES `dept` (`deptId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of emp
-- ----------------------------
INSERT INTO `emp` VALUES ('2', '魏英杰', '520520', '1', '2014-12-03 14:03:57');
INSERT INTO `emp` VALUES ('5', '张小明', '234567', '3', '2014-12-10 14:05:15');
INSERT INTO `emp` VALUES ('6', '张三', '3453321', '5', '2014-12-10 08:52:26');
INSERT INTO `emp` VALUES ('9', '王五', '123456', '1', '2015-01-05 16:04:04');

-- ----------------------------
-- Table structure for `order`
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `ORDER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_NAME` varchar(255) DEFAULT NULL,
  `TREE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ORDER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('1', '订单一', '1');

-- ----------------------------
-- Table structure for `tree`
-- ----------------------------
DROP TABLE IF EXISTS `tree`;
CREATE TABLE `tree` (
  `TREE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TREE_NAME` varchar(255) NOT NULL,
  `TREE_PRICE` double NOT NULL,
  `TREE_COUNT` int(11) NOT NULL,
  PRIMARY KEY (`TREE_ID`),
  UNIQUE KEY `UQ_treeName` (`TREE_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tree
-- ----------------------------
INSERT INTO `tree` VALUES ('1', '杨树', '20', '10');

-- ----------------------------
-- Table structure for `t_employee`
-- ----------------------------
DROP TABLE IF EXISTS `t_employee`;
CREATE TABLE `t_employee` (
  `EMPLOYEE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMPLOYEE_NAME` varchar(6) DEFAULT NULL,
  `EMPLOYEE_PSSWORD` varchar(6) DEFAULT NULL,
  `EMPLOYRR_TIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`EMPLOYEE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_employee
-- ----------------------------
INSERT INTO `t_employee` VALUES ('1', '张五3', '11111', '2014-12-29 16:03:24');
INSERT INTO `t_employee` VALUES ('2', '张四', '233455', '2014-12-17 14:12:51');
INSERT INTO `t_employee` VALUES ('3', '王五', '123333', '2014-12-23 14:13:04');

-- ----------------------------
-- Table structure for `t_stu`
-- ----------------------------
DROP TABLE IF EXISTS `t_stu`;
CREATE TABLE `t_stu` (
  `stuId` int(11) NOT NULL AUTO_INCREMENT,
  `stuName` varchar(10) DEFAULT NULL,
  `stuSex` varchar(2) DEFAULT NULL,
  `stuTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`stuId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_stu
-- ----------------------------
INSERT INTO `t_stu` VALUES ('2', '张博文', '男', '2014-12-18 14:50:35');
INSERT INTO `t_stu` VALUES ('3', '范冰冰', '女', '2014-12-18 14:50:49');
INSERT INTO `t_stu` VALUES ('4', '林志玲', '女', '2014-12-18 14:51:03');
INSERT INTO `t_stu` VALUES ('5', '孙悟空', '男', '2014-12-18 14:51:17');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'userId为用户表的主键',
  `userName` varchar(10) DEFAULT NULL COMMENT '用户名为不允许出现重复的',
  `userSex` varchar(2) DEFAULT NULL COMMENT '用户性别是男或女',
  `userAge` int(11) DEFAULT NULL COMMENT '用户年龄为int类型允许为空',
  `userTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '插入的是本地的时间',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('5', '王五', '男', '33', '2014-12-24 08:59:34');
INSERT INTO `user` VALUES ('13', '张1', '男', '23', '2014-12-23 16:00:43');
INSERT INTO `user` VALUES ('14', '王五', '男', '12', '2014-12-24 10:35:31');
INSERT INTO `user` VALUES ('15', 'aas', '男', '12', '2014-12-24 10:36:36');
INSERT INTO `user` VALUES ('16', 'aas', '男', '22', '2014-12-24 10:37:26');
INSERT INTO `user` VALUES ('17', '111', '女', '11', '2014-12-24 10:41:02');
INSERT INTO `user` VALUES ('18', 'aa', '女', '11', '2014-12-24 10:42:28');
INSERT INTO `user` VALUES ('19', 'q', '男', '11', '2014-12-24 10:46:25');
INSERT INTO `user` VALUES ('20', 'aas', '男', '11', '2014-12-24 10:47:33');
INSERT INTO `user` VALUES ('21', 'www', '男', '11', '2014-12-24 10:48:29');
INSERT INTO `user` VALUES ('22', 'aa', '男', '11', '2014-12-24 10:50:02');
INSERT INTO `user` VALUES ('23', 'bb', '女', '12', '2014-12-24 10:52:29');
INSERT INTO `user` VALUES ('24', 'bb', '女', '12', '2014-12-24 10:53:35');
INSERT INTO `user` VALUES ('25', 'bb', '女', '12', '2014-12-24 10:53:46');
INSERT INTO `user` VALUES ('26', 'bb', '女', '12', '2014-12-24 10:53:59');
INSERT INTO `user` VALUES ('27', 'bb', '女', '12', '2014-12-24 10:54:06');
INSERT INTO `user` VALUES ('28', 'eee', '男', '23', '2014-12-24 10:56:12');
INSERT INTO `user` VALUES ('29', 'eee', '男', '23', '2014-12-24 10:56:22');
INSERT INTO `user` VALUES ('30', 'eeeee', '女', '12', '2014-12-24 10:56:37');
INSERT INTO `user` VALUES ('31', 'e1', '男', '12', '2014-12-24 10:58:16');
INSERT INTO `user` VALUES ('32', 'e2', '男', '12', '2014-12-24 11:00:18');
INSERT INTO `user` VALUES ('33', '张飞', '男', '33', '2014-12-29 09:27:08');
