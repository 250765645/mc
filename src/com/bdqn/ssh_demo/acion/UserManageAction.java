package com.bdqn.ssh_demo.acion;

import java.util.ArrayList;
import java.util.List;

import com.bdqn.ssh_demo.dao.IUserManageDao;
import com.bdqn.ssh_demo.entity.User;
import com.opensymphony.xwork2.ActionSupport;

public class UserManageAction extends ActionSupport {
	private IUserManageDao userManageDAO;
	private List<User> list = new ArrayList<User>();
	private User u = new User();
	private String username;
	private String usersex;
	private int userage;
	private int userId;

	public String selectUserList() {
		list = userManageDAO.selectUserList();
		return "success_showList";
	}

	public String selectUserById() {
		u = userManageDAO.selectUserById(userId);
		return "success_user";
	}
	
	
	public String updateUserById(){
		System.out.println(userId+username+usersex+userage);
		userManageDAO.updateUserById(userId, username, usersex, userage);
		return this.selectUserList();
	}
	
	public String deleteUserById(){
		userManageDAO.deleteUserById(userId);
		return this.selectUserList();
	}

	public IUserManageDao getUserManageDAO() {
		return userManageDAO;
	}

	public void setUserManageDAO(IUserManageDao userManageDAO) {
		this.userManageDAO = userManageDAO;
	}

	public List<User> getList() {
		return list;
	}

	public void setList(List<User> list) {
		this.list = list;
	}

	public User getU() {
		return u;
	}

	public void setU(User u) {
		this.u = u;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsersex() {
		return usersex;
	}

	public void setUsersex(String usersex) {
		this.usersex = usersex;
	}

	public int getUserage() {
		return userage;
	}

	public void setUserage(int userage) {
		this.userage = userage;
	}


}
