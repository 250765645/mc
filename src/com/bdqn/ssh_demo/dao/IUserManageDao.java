package com.bdqn.ssh_demo.dao;

import java.util.List;

import com.bdqn.ssh_demo.entity.User;

public interface IUserManageDao {
	
	public List<User> selectUserList();
	
	
	public User selectUserById(int userId);
	
	public void updateUserById(int userId,String userName,String userSex,int userAge);
	
	public void deleteUserById(int userId);

}
