package com.bdqn.ssh_demo.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bdqn.ssh_demo.dao.IUserManageDao;
import com.bdqn.ssh_demo.entity.User;

public class IUserManageDaoImpl extends HibernateDaoSupport implements IUserManageDao
{

	@Override
	public List<User> selectUserList() {
		List<User> list=null;
		// TODO Auto-generated method stub
		//return this.getHibernateTemplate().find("from User");
		list=this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				return session.createQuery("from User").list();
			}
		});
		return list;
	}

	@Override
	public User selectUserById(int userId) {
		List<User> list=this.getHibernateTemplate().find("from User where UserId="+userId);
		User u=(User)list.get(0);
		return u;
	}

	@Override
	public void updateUserById(int userId, String userName, String userSex,
			int userAge) {
	    User u=(User)this.getHibernateTemplate().find("from User where UserId="+userId).get(0);
		u.setUserName(userName);
		u.setUserSex(userSex);
		u.setUserAge(userAge);
		this.getHibernateTemplate().update(u);
		
		
	}

	@Override
	public void deleteUserById(int userId) {
		 User u=(User)this.getHibernateTemplate().find("from User where UserId="+userId).get(0);
		 this.getHibernateTemplate().delete(u);
	}
	
	
//	public void work(final int userId){
//		this.getHibernateTemplate().execute(new HibernateCallback() {
//
//			@Override
//			public Object doInHibernate(Session session)
//					throws HibernateException, SQLException {
//			
//				return session.createQuery("from User where UserId="+userId);
//			}
//		});
//		
//	}

}
