package com.bdqn.ssh_demo.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * User entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "user", catalog = "test")
public class User implements java.io.Serializable {

	// Fields

	private Integer userId;
	private String userName;
	private String userSex;
	private Integer userAge;
	private Timestamp userTime;

	// Constructors

	/** default constructor */
	public User() {
	}

	/** full constructor */
	public User(String userName, String userSex, Integer userAge,
			Timestamp userTime) {
		this.userName = userName;
		this.userSex = userSex;
		this.userAge = userAge;
		this.userTime = userTime;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "userId", unique = true, nullable = false)
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "userName", length = 10)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "userSex", length = 2)
	public String getUserSex() {
		return this.userSex;
	}

	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}

	@Column(name = "userAge")
	public Integer getUserAge() {
		return this.userAge;
	}

	public void setUserAge(Integer userAge) {
		this.userAge = userAge;
	}

	@Column(name = "userTime", length = 0)
	public Timestamp getUserTime() {
		return this.userTime;
	}

	public void setUserTime(Timestamp userTime) {
		this.userTime = userTime;
	}

}